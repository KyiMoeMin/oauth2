package demo.oauth2.model;

public class User {

	Integer id;
	String userName,password,role;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return "ROLE_USER";
	}
	public void setRole(String role) {
		this.role = role;
	}

	
	
}
