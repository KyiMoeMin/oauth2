package demo.oauth2.manager;

import javax.annotation.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import demo.oauth2.mapper.UserMapper;
import demo.oauth2.model.User;

@ManagedBean
public class UserManager {

	@Autowired
	UserMapper userMapper;
	
	public User findByUserName(String name) {
		return userMapper.findByUserName(name);
	}
}
