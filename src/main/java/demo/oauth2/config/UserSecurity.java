package demo.oauth2.config;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

@Service
public class UserSecurity {

    public boolean hasUserName(Authentication authentication, String userName) {
        User user = (User) authentication.getPrincipal();
        return user.getUsername().equals(userName);
    }
}
