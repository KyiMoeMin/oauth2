package demo.oauth2.config;

import java.util.Collection;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import demo.oauth2.manager.UserManager;
import demo.oauth2.model.User;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	UserManager userManager;
	
	@Override
	public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
		System.err.println("test in this");
		User user;
		user=userManager.findByUserName(name);
		if(user==null)
			throw new UsernameNotFoundException("Invalid user name or password");
		return new org.springframework.security.core.userdetails.User(
				user.getUserName(),
				user.getPassword(),
				(Collection<? extends GrantedAuthority>) Arrays.asList(new SimpleGrantedAuthority(user.getRole())));
	}


}
