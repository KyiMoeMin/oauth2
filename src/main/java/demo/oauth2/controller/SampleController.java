package demo.oauth2.controller;

import org.springframework.web.bind.annotation.*;

import demo.oauth2.model.User;


@RestController
public class SampleController {

	@GetMapping("/hi")
	public String hello() {
		return "Hello there!";
	}
	
	public User getUser() {
		return null;
	}

	@GetMapping("/test/{userName}")
	public String test(@PathVariable String userName){
		return "hello "+userName;
	}
	
	@DeleteMapping("/test/{userName}")
	public String delete(@PathVariable String userName) {
		return "delete "+userName;
	}
	
	@PostMapping("/hi")
	public void hi() {
		
	}
}
