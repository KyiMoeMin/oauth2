package demo.oauth2.interceptor;

import java.io.IOException;
import java.io.Serializable;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import demo.oauth2.model.Log;
import demo.oauth2.repository.LogRepository;

public class LogInterceptor extends HandlerInterceptorAdapter {
	
	@Autowired
	public LogRepository logRepository;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
		log(request,response);
		return true;
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) throws Exception {
		super.afterCompletion(request, response, handler, ex);
	}
	
	public void log(HttpServletRequest request,HttpServletResponse response) throws IOException {
		
		System.err.println("request "+request.getRemoteAddr()+"\n host"
		+request.getRemoteHost()+"\n method"
		+request.getMethod()+"\n user"
		+request.getRemoteUser()+"\n url"
		+request.getRequestURI()+" \n payload"
		+request.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
		Log log = getLog(request);
		logRepository.save(log);
	}
	
	public Log getLog(HttpServletRequest r) throws IOException {
		Log log = new Log();
			log.setIp(r.getRemoteAddr());
			log.setMethod(r.getMethod());
			log.setUserName(r.getRemoteUser());
			log.setUrl(r.getRequestURI());
			if(r.getReader().lines()!=null)
			log.setBody(r.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
		return log;
	}
}
