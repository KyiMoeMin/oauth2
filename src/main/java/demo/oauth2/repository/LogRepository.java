package demo.oauth2.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import demo.oauth2.model.Log;

@SuppressWarnings("unchecked")
@Repository
public interface LogRepository extends CrudRepository<Log,Integer> {

	
//	public Log save(Log log);
}
