package demo.oauth2.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import demo.oauth2.model.User;

@Mapper
public interface UserMapper {
	
	public final String TABLE="Users";

	@Select("select * from "+TABLE+" where userName=#{name}")
	public User findByUserName(String name);
}
